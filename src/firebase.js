import firebase from 'firebase/app'

const firebaseConfig =
{
    apiKey: "AIzaSyAfFhs0UWH7ATggz1QkpW1T7QvT7S2mDXw",
    authDomain: "react-training-65352.firebaseapp.com",
    projectId: "react-training-65352",
    storageBucket: "react-training-65352.appspot.com",
    messagingSenderId: "1075680787593",
    appId: "1:1075680787593:web:94e86257e9f5a15b491a58"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig)